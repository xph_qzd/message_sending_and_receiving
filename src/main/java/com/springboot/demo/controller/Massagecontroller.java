package com.springboot.demo.controller;


import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.demo.serviceImpl.Producer;

@RestController
public class Massagecontroller {
	@Autowired
    private Producer producer;
	@RequestMapping(value="/send",method=RequestMethod.GET)
	@ResponseBody
	public String send(){
		String message="注册";
		producer.send(message);
		return message;	
	}
	@RequestMapping("/send1")
	public String send1(){
		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		String conter="时间："+format.format(date);
		producer.send(conter);
		return conter;
	}
}
