package com.springboot.demo.entity;

import java.io.Serializable;

import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.ContentTypeDelegatingMessageConverter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;

public class Messages implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String email;
	private String username;
	private String randompwd;
	private String text;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getRandompwd() {
		return randompwd;
	}
	public void setRandompwd(String randompwd) {
		this.randompwd = randompwd;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Messages() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Messages(String email, String username, String randompwd, String text) {
		super();
		this.email = email;
		this.username = username;
		this.randompwd = randompwd;
		this.text = text;
	}
	
	    @Override
	public String toString() {
		return "Messages [email=" + email + ", username=" + username + ", randompwd=" + randompwd + ", text=" + text
				+ "]";
	}
		@Bean
	    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(ConnectionFactory connectionFactory, MessageConverter messageConverter) {
	        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
	        factory.setConnectionFactory(connectionFactory);
	        factory.setMessageConverter(messageConverter);
	        return factory;
	    }
	    @Bean
	    public MessageConverter messageConverter() {
	        return new ContentTypeDelegatingMessageConverter(new Jackson2JsonMessageConverter());
	    }
	    @Bean
	    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory, MessageConverter messageConverter) {
	        RabbitTemplate template = new RabbitTemplate(connectionFactory);
	        template.setMessageConverter(messageConverter);
	        return template;
	    }
    
}
