package com.springboot.demo.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.springboot.demo.entity.Messages;

public class ByteObj {
     //把baty数组转化为对象
	 public static Object getObjectFromBytes(byte[] objBytes) throws Exception {
		 if (objBytes == null || objBytes.length == 0) { 
			 return null; 
			 } 
		 ByteArrayInputStream bi = new ByteArrayInputStream(objBytes); 
		 ObjectInputStream oi = new ObjectInputStream(bi); 
		 return oi.readObject(); 
		 }
	 //把对象转化为数组
	 public static byte[] getByteFromObject(Object obj) throws IOException{
		 byte[] bytes=null;
		 if(obj==null){
			 return null;
		 }
		    ByteArrayOutputStream bo = new ByteArrayOutputStream();  
	        ObjectOutputStream oo = new ObjectOutputStream(bo);  
	        oo.writeObject(obj);  
	        bytes = bo.toByteArray();  
		 return bytes;
	 }
}
