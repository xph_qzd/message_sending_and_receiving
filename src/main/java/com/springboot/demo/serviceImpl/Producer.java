package com.springboot.demo.serviceImpl;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 生产者产生消息
 * @author Administrator
 * message 消息
 */
@Component
public class Producer {
	@Autowired
	private RabbitTemplate rabbitemplate;
	
	public void send(String message){
		//CorrelationData correlationData=new  CorrelationData(uuid);
		//指定交换机    队列      
		System.out.println("发送消息为"+message);
		rabbitemplate.convertAndSend("Exchangefirst","loginfrist",message);	
		}
}
