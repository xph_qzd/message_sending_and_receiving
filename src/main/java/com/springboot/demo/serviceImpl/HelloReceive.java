package com.springboot.demo.serviceImpl;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

/**
 * 消息接收，并发送邮件
 * @author Administrator
 *
 */
@Component
@RabbitListener(queues="loginfrist")
public class HelloReceive {
	@Autowired
	private JavaMailSender mailSender;
	@Value("${mail.fromMail.addr}")
	private String from;
	//登录
	@RabbitHandler
	public void processA(String a) {
		System.out.println("接受消息为"+a);
		/*SimpleMailMessage message = new SimpleMailMessage();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
		String time = df.format(new Date());// new Date()为获取当前系统时间
		System.out.println(time);
		message.setFrom(from);
		message.setTo(a);
		message.setSubject("登录");
		message.setText("你与"+time+"登录");
		try {
			mailSender.send(message);
			System.out.println("简单邮件发送成功！");
		}catch (Exception e) {
			System.out.println("发送简单邮件时发生异常！"+e);
		}*/
    }

}
