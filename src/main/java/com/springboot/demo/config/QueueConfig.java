package com.springboot.demo.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 队列配置
 * @author Administrator
 *
 */
@Configuration
public class QueueConfig {
	/**
	 * 队列
	 * @return
	 */
	@Bean
	public Queue loginQueue(){
		 return new Queue("loginfrist");
	}
}
