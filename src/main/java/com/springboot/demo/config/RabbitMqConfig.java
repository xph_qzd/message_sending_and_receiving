package com.springboot.demo.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;



public class RabbitMqConfig {
		
		@Autowired
		private QueueConfig queueConfig;
		@Autowired
	    private ExchangeConfig exchangeConfig;
		
		 /*
	    	将登录消息队列和交换机进行绑定
		  */
	   
		public Binding loginbindingExchangeMessage(){
			return BindingBuilder.bind(queueConfig.loginQueue()).to(exchangeConfig.topicExchange()).with("loginfrist");
		}
		
}
