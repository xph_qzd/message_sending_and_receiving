package com.springboot.demo.config;

import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 交换机配置
 * @author Administrator
 *
 */
@Configuration
public class ExchangeConfig {

	/**
	 * 交换机
	 * @return
	 */
	@Bean
	public TopicExchange topicExchange(){
		return new TopicExchange("Exchangefirst");
	}
}
